[33mcommit 68df8a32caff0078b89c0a5703b516ac9adec86f[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m)[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 7 08:58:00 2017 -0300

    corrigi o carremento do sweetalert, removido o zend-db, adicionado o zend-json

[33mcommit e41f7e71b8cf6683b37e5f1e99c8524651ab73b7[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 23:54:07 2017 -0300

    login por enquanto só esta considernado o usuario e não a senha, foi adicionada a biblioteca sweetalert;

[33mcommit 7bdf974e5a67941a2a4d677e1023b8a09e0a2855[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 18:29:53 2017 -0300

    adicionei a library de ACL no composer

[33mcommit bd6497edaab9f2b04f7a221cde68876ea9a34fc3[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 18:21:00 2017 -0300

    export inicial da base de dados

[33mcommit d66541bca2c4ee1bd8a6b0fe143147b2a7a89d19[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 18:18:25 2017 -0300

    Criei a tabela no banco de dados e estruturei o fluxo inicial do login

[33mcommit b281d42e3ccff161e7a73f59d8072cc702957e8c[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 17:18:10 2017 -0300

    Adicionei o fluentPdo para utilizar nas models, assim não precisará usar o service locator do zend

[33mcommit c9ac9bd6ceb8cf959f6203e6fb595ba829fd4499[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 02:03:50 2017 -0300

    Edição na estrutura de pastas, organizei melhor e consegui criar o modulo Base, porém a função getServiceLocator() foi descontinuado no zend 3, preciso arrumar isto na classe CustomActionController, que é a classe que extende todas as controllers da aplicação. O problema esta atualmente na linha 65 da CustomActionController().

[33mcommit f5c0072843642179c2959c54d89fdf114eeab215[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 02:02:52 2017 -0300

    Edição na estrutura de pastas, organizei melhor e consegui criar o modulo Base, porém a função getServiceLocator() foi descontinuado no zend 3, preciso arrumar isto na classe CustomActionController, que é a classe que extende todas as controllers da aplicação. O problema esta atualmente na linha 65 da CustomActionController().

[33mcommit aaaff03fc1c147dd05f1971f18231f0d9927aff7[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Thu Oct 5 22:41:13 2017 -0300

    criado layout de login, estrutura de pastas foi alterada, e foi criado a CustomAbstractTableGateway, que será extendida em todas as models

[33mcommit eca45b22b65026d435fdddff4256621082212fdd[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Wed Oct 4 17:19:14 2017 -0300

    primeiro commit
