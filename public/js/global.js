var urlAtual = window.location.href;
var urlAtual = getUrlAtual(urlAtual);
var baseUrl = window.location.host;
var protocol = window.location.protocol;
if (!urlAtual.split('/')[4]) {
    controllerAtual = 'index';
} else {
    var controllerAtual = urlAtual.split('/')[4];
}
url = urlAtual;
urlPura = url.replace(/#[^#]*$/, "").replace(/\?[^\?]*$/, "").replace(/^https:/, "http:");
var moduloAtual = urlAtual.split('/')[3];
if (controllerAtual.indexOf('content_only') != -1) {
    controllerAtual = controllerAtual.split('?')[0];
} else {
    controllerAtual = urlAtual.split('/')[4];
}

var _erroPadraoAjax = 'Ocorreu ao tentar executar a ação, entre em contato com a equipe técnica.';
var moduloAtual = urlAtual.split('/')[3];
function getUrlAtual(urlAtual) {
    if (urlAtual.indexOf('?') > 0) {
        urlAtual = urlAtual.split('?');
        if (urlAtual[1].indexOf('/') > 0) {
            param = urlAtual[1].split('/');
            idParam = param[1];
            urlAtual = urlAtual[0] + '/' + idParam + '?' + param[0];
        } else {
            urlAtual = urlAtual[0] + '?' + urlAtual[1];
        }
    }
    return urlAtual;
}


var urlAtual = window.location.href;
var urlAtual = getUrlAtual(urlAtual);
var baseUrl = window.location.host;
var protocol = window.location.protocol;
var htmlMsgAguarde = "";
htmlMsgAguarde += "";
htmlMsgAguarde += "    <!-- Modal da mensagem do aguarde -->";
htmlMsgAguarde += "    <div class=\"modal fade modalSistema\" id=\"modalMensagem\" tabindex=\"-1\" role=\"aguarde\" aria-labelledby=\"aguarde\" style=\"z-index:9999;position: absolute;  top: 50%;left: 50%;transform: translate(-50%, -50%);\">";
htmlMsgAguarde += "        <div class=\"modal-dialog\" role=\"document\" style=\"width:85px\">";
htmlMsgAguarde += "            <div class=\"modal-content\" style=\"width:85px\">";
htmlMsgAguarde += "                <div class=\"modal-body\" style=\"font-size: 32px;font-weight: bold; text-align: center\">";
htmlMsgAguarde += "                    <div class=\"lds-css ng-scope\">";
htmlMsgAguarde += "                    <div style=\"width:100%;height:100%\" class=\"lds-dual-ring\">";
htmlMsgAguarde += "                <div><\/div>";
htmlMsgAguarde += "                <\/div>";
htmlMsgAguarde += "                <\/div>";
htmlMsgAguarde += "                <\/div>";
htmlMsgAguarde += "            <\/div>";
htmlMsgAguarde += "        <\/div>";
htmlMsgAguarde += "    <\/div>";
function OpenMsgAguarde() {
    $("#div-modalMensagem").html(htmlMsgAguarde);
    $("#modalMensagem").modal();
}

function CloseMsgAguarde() {
    $("#modalMensagem").modal("hide");
    $(".modal-backdrop").remove();
    $("body").removeAttr("style");
}

/**
 * @param {string} tipo:    - info <br />
 *                          - warning <br />
 *                          - success <br />
 *                          - error <br />
 *                          - question <br />
 *                          
 * @param {bool} exibirButtonConfirm: exibe ou não o botão no final da mensagem, texto padrão 'OK'
 * @param {string} tituloBotaoConfirm: mensagem do texto do botão de confirmação, padrão: 'OK'
 */
function openMsg(titulo = "", mensagem = "", tipo = "info", exibirButtonConfirm = true, tituloBotaoConfirm = 'OK') {
    swal({
        title: titulo,
        text: mensagem,
        type: tipo,
        showConfirmButton: exibirButtonConfirm,
        confirmButtonText: tituloBotaoConfirm
    });
}

function _initDateRangePicker() {
    $(".daterange").daterangepicker({
        locale: {
            format: 'DD/MM/YYYY',
            locale: 'pt-br',
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "De",
            "toLabel": "Até",
            "customRangeLabel": "Personalizado",
        },
    });
}

function _initDatePicker() {
    $(".date").datepicker();
}

function formDefault(formIdentification, urlRedirectSuccess) {
    if ($(formIdentification).valid()) {
        OpenMsgAguarde();
        $(formIdentification).ajaxSubmit({
            type: 'POST',
            dataType: 'json',
            success: function (r) {
                CloseMsgAguarde();
                if (r.type === 'success') {
                    openMsg('Sucesso', r.message, 'success', true, 'Fechar');
                    swal.showLoading();
                    setTimeout(function () {
                        window.location = urlRedirectSuccess;
                    }, 2000);
                } else {
                    openMsg('Ops', r.message, 'error', true, 'Fechar');
                }
            },
            error: function (r) {
                if (empty(r.message)) {
                    r.message = _erroPadraoAjax;
                }
                openMsg('Ops', r.message, 'error', true, 'Fechar');
            }
        });
    } else {
        openMsg('Ops', 'Alguns campos não foram preenchidos corretamente, verifique novamente', 'warning', true, 'Fechar');
        return false;
    }
}


jQuery.validator.setDefaults({
    highlight: function (element, errorClass, validClass) {
        if (element.type === "radio") {
            this.findByName(element.name).addClass(errorClass).removeClass(validClass);
        } else {
            $(element).closest('.form-group').removeClass('has-success has-feedback').addClass('has-error has-feedback');
            $(element).closest('.form-group').find('i.fa').remove();
            $(element).closest('.form-group').append('<i class="fa fa-exclamation fa-lg form-control-feedback"></i>');
        }
    },
    unhighlight: function (element, errorClass, validClass) {
        if (element.type === "radio") {
            this.findByName(element.name).removeClass(errorClass).addClass(validClass);
        } else {
            $(element).closest('.form-group').removeClass('has-error has-feedback').addClass('has-success has-feedback');
            $(element).closest('.form-group').find('i.fa').remove();
            $(element).closest('.form-group').append('<i style="line-height:1.75em!important" class="fa fa-check fa-lg form-control-feedback"></i>');
        }
    }
});