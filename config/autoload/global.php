<?php

$db = array(
    'database' => 'eloja',
    'username' => 'root',
    'password' => 'root',
    'hostname' => 'localhost'
);

return array(
    'service_manager' => array(
        'factories' => array(
            'zend_db_adapter' => function($sm) use ($db) {
                return new Zend\Db\Adapter\Adapter(array(
                    'driver' => 'pdo',
                    'dsn' => 'mysql:dbname=' . $db['database'] . ';host=' . $db['hostname'],
                    'database' => $db['database'],
                    'username' => $db['username'],
                    'password' => $db['password'],
                    'hostname' => $db['hostname'],
                    'driver_options' => array(
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                    ),
                ));
            }
        )
    )
);
