<?php

namespace Portal\Service;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\CustomAbstractTableGateway;
use Zend\Db\TableGateway\Feature\FeatureSet;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Tblogin extends CustomAbstractTableGateway {

    protected $table = 'tblogin';
    protected $_primary = 'id_login';

    public function __construct() {
        $this->featureSet = new FeatureSet();
        $this->featureSet->addFeature(new GlobalAdapterFeature());
        $this->initialize();
    }

    public function fetchAll($params = array()) {

//        $params = array_filter($params);

        $sql = new Sql($this->adapter);
        $query = $sql->select(array('c' => $this->table));

        if (!empty($params['id_cupom'])) {
            $query->where("id_cupom = '{$params['id_cupom']}'");
        }

        if (!empty($params['id_unidade'])) {
            $query->where("id_unidade = '{$params['id_unidade']}'");
        }

        if (!empty($params['tx_nome'])) {
            $query->where("tx_nome LIKE '%{$params['tx_nome']}%'");
        }

        if (!empty($params['tx_codigo'])) {
            $query->where("tx_codigo LIKE '{$params['tx_codigo']}'");
        }

        if (!empty($params['dt_validade_entre_inicio'])) {
            $query->where("dt_validade_inicio >= '{$params['dt_validade_entre_inicio']}'");
        }

        if (!empty($params['dt_validade_entre_fim'])) {
            $query->where("dt_validade_fim <= '{$params['dt_validade_entre_fim']}'");
        }

        if (!empty($params['only_disponiveis'])) {
            $query->where('nr_quantidadeutilizado < nr_quantidade');
        }

        $query = $sql->buildSqlString($query, $this->adapter);

        $result = $this->executeSql($query);

        return $result;
    }

}
