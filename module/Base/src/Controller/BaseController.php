<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Base\Controller;

use Zend\Mvc\Controller\CustomActionController;

class BaseController extends CustomActionController {

    public function dd($data,$die=true) {        
        print "<pre>";
        if (is_array($data)) {
            print_r($data);
        } else if (is_string($data)) {
            echo $data;
        } else if (is_bool($data)) {
            var_export($data);
        } else {
            var_dump($data);
        }
        print "</pre>";

        if ($die) {
            die;
        }
    }

}
