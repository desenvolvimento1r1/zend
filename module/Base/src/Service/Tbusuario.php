<?php

namespace Base\Service;

use Zend\Db\TableGateway\CustomAbstractTableGateway;

class Tbusuario extends CustomAbstractTableGateway {

    protected $table = 'tbusuario';
    protected $_primary = 'id_usuario';
    protected $sql;

    public function __construct() {
        $this->sql = $this->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->getConnection();
        $query = $db->from($this->table);

        if (!empty($params['usuario'])) {
            $query->where("tx_login LIKE '{$params['usuario']}'");
        }

        if (!empty($params['senha'])) {
            $query->where("senha LIKE '{$params['senha']}'");
        }

        if (!empty($params['fb_userid'])) {
            $query->where("fb_userid = '{$params['fb_userid']}'");
        }
        
        return $query->fetchAll();
    }

}
