<?php

namespace Site\Controller;

use Base\Service\Tbusuario;
use Exception;
use Zend\Json\Json;
use Zend\Mvc\Controller\CustomActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class IndexController extends CustomActionController {

    public function __construct() {
        $this->module = "site";
    }

    public function indexAction() {
        return new ViewModel(array(
        ));
    }

    public function verificaLoginAction() {
        $username = $this->params()->fromPost('username');
        $password = password_hash($this->params()->fromPost('password'), PASSWORD_DEFAULT);

        try {

            $Tbusuario = new Tbusuario();
            $usuario = $Tbusuario->fetchAll(array(
                'usuario' => $username,
//                'senha' => $password
            ));

            if (!empty($usuario)) {

                $this->setAcl();
                $return['type'] = 'success';
                $return['message'] = 'Entrando';
            } else {
                Throw new Exception('Usuário/senha não encontrados.');
            }
        } catch (Exception $ex) {
            $return['type'] = 'error';
            $return['message'] = $ex->getMessage();
        }

//        print_r($return);die;

        $json = new Json();
        echo $json->encode($return);
        die;
    }

    public function cadastrarAction() {
        $data = $this->params()->fromPost();

        $Tbusuario = new Tbusuario();

        try {
            $this->dd($data);
            $return['type'] = 'success';
            $return['message'] = 'Entrando';
        } catch (Exception $ex) {
            $return['type'] = 'error';
            $return['message'] = $ex->getMessage();
        }


        $json = new Json();
        echo $json->encode($return);
        die;
    }

    public function loginFbAction() {
        $data = $this->params()->fromPost();

//        (new \Base\Controller\BaseController())->dd(true, $data);

        if (!isset($data['middle_name'])) {
            $data['middle_name'] = '';
        }

        if (!isset($data['last_name'])) {
            $data['last_name'] = '';
        }

        if (!isset($data['email'])) {
            $data['email'] = '';
        }

        $Tbusuario = new Tbusuario();

        $usuarioFb = $Tbusuario->fetchAll(array('fb_userid' => $data['id'], 'or_tx_email' => $data['email']));

        try {
            if (empty($usuarioFb)) {
                $id = $Tbusuario->save(array(
                    'tx_nomeusuario' => $data['first_name'] . $data['middle_name'] . $data['last_name'],
                    'tx_email' => $data['email'],
                    'fb_userid' => $data['id'],
                    'st_tipologin' => 'F'
                ));
            }

            $this->setAcl();

            $return['type'] = 'success';
            $return['message'] = 'Entrando';
        } catch (Exception $ex) {
            $return['type'] = 'error';
            $return['message'] = $ex->getMessage();
        }


        $json = new Json();
        echo $json->encode($return);
        die;
    }

    private function setAcl() {

//        $acl = new Acl();
//
//        $acl->addResource('cadastro-usuario')
//                ->addResource('alterar-usuario');
//
//        $acl->addRole('usuario');
//
//        $acl->allow('usuario', 'cadastro-usuario');
//
//        echo var_export($acl->isAllowed('usuario', 'cadastro-usuario'));
//        die;
    }

}
