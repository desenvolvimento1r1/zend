[33mcommit 6df2f5cc8db39b73d6e38c2dd69f388078da5bb6[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m)[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 14 13:00:56 2017 -0300

    x

[33mcommit be7d4b3f7cd7d7dcf3571246a809f9e4be464847[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 14 12:59:56 2017 -0300

    Adicionei o template do liteADM no portal, somente na index. Tambem foi adicionado a tradução do datepicker, datepickerange. Dentro das controller agora é obrigatório especificar o modulo por uma variavel chamado $this-

[33mcommit 7a8bbf44d49c2d16d7611dc8f86149ad1ebcfcee[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Thu Oct 12 07:08:40 2017 -0300

    Adicionei a rotina de login no modulo do Site, retirei do modulo Portal

[33mcommit f882f1ed64861acfa86fa6de1e70891358a126ef[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sun Oct 8 15:20:53 2017 -0300

    Base de dados do 'logar com facebook'

[33mcommit 35fe641da5d887496487f8ab64322d239f2c9766[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sun Oct 8 15:19:32 2017 -0300

    Realizado o 'Logar com Facebook' (feito em js)

[33mcommit 08f28eb6d08553bf7b099e5bd2f36a900301fc58[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 7 18:59:03 2017 -0300

    funcao de mensagem criada no global, e funcao de js no view melhorada

[33mcommit e188c44e306055e88a2c9d0cfac76118c06f1716[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 7 13:13:15 2017 -0300

    Revert "Revert "Adicionei o font awsome nas bibliotecas para utilizar juntamente como validate do jquery, que tambem foi adicionado nesta versao.""
    
    This reverts commit 3158599f066023845eee4febd09267228c6e3f84.

[33mcommit 3158599f066023845eee4febd09267228c6e3f84[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 7 13:12:05 2017 -0300

    Revert "Adicionei o font awsome nas bibliotecas para utilizar juntamente como validate do jquery, que tambem foi adicionado nesta versao."
    
    This reverts commit 014548573fa22c4cc0c6c53364a369c66df5df6a.

[33mcommit c98c63ebc9beec6f94cd919104936000816adb51[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 7 13:12:05 2017 -0300

    Revert "Alterei o nome da palavra 'requerido' para 'obrigatório' na traducao do jquery validate"
    
    This reverts commit 7a0db55a74675a35cdb4a1fdd02751098de12a61.

[33mcommit 7a0db55a74675a35cdb4a1fdd02751098de12a61[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 7 13:07:54 2017 -0300

    Alterei o nome da palavra 'requerido' para 'obrigatório' na traducao do jquery validate

[33mcommit 014548573fa22c4cc0c6c53364a369c66df5df6a[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 7 13:06:48 2017 -0300

    Adicionei o font awsome nas bibliotecas para utilizar juntamente como validate do jquery, que tambem foi adicionado nesta versao.

[33mcommit 68df8a32caff0078b89c0a5703b516ac9adec86f[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Sat Oct 7 08:58:00 2017 -0300

    corrigi o carremento do sweetalert, removido o zend-db, adicionado o zend-json

[33mcommit e41f7e71b8cf6683b37e5f1e99c8524651ab73b7[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 23:54:07 2017 -0300

    login por enquanto só esta considernado o usuario e não a senha, foi adicionada a biblioteca sweetalert;

[33mcommit 7bdf974e5a67941a2a4d677e1023b8a09e0a2855[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 18:29:53 2017 -0300

    adicionei a library de ACL no composer

[33mcommit bd6497edaab9f2b04f7a221cde68876ea9a34fc3[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 18:21:00 2017 -0300

    export inicial da base de dados

[33mcommit d66541bca2c4ee1bd8a6b0fe143147b2a7a89d19[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 18:18:25 2017 -0300

    Criei a tabela no banco de dados e estruturei o fluxo inicial do login

[33mcommit b281d42e3ccff161e7a73f59d8072cc702957e8c[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 17:18:10 2017 -0300

    Adicionei o fluentPdo para utilizar nas models, assim não precisará usar o service locator do zend

[33mcommit c9ac9bd6ceb8cf959f6203e6fb595ba829fd4499[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 02:03:50 2017 -0300

    Edição na estrutura de pastas, organizei melhor e consegui criar o modulo Base, porém a função getServiceLocator() foi descontinuado no zend 3, preciso arrumar isto na classe CustomActionController, que é a classe que extende todas as controllers da aplicação. O problema esta atualmente na linha 65 da CustomActionController().

[33mcommit f5c0072843642179c2959c54d89fdf114eeab215[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Fri Oct 6 02:02:52 2017 -0300

    Edição na estrutura de pastas, organizei melhor e consegui criar o modulo Base, porém a função getServiceLocator() foi descontinuado no zend 3, preciso arrumar isto na classe CustomActionController, que é a classe que extende todas as controllers da aplicação. O problema esta atualmente na linha 65 da CustomActionController().

[33mcommit aaaff03fc1c147dd05f1971f18231f0d9927aff7[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Thu Oct 5 22:41:13 2017 -0300

    criado layout de login, estrutura de pastas foi alterada, e foi criado a CustomAbstractTableGateway, que será extendida em todas as models

[33mcommit eca45b22b65026d435fdddff4256621082212fdd[m
Author: cgr <caique.galvanin@gmail.com>
Date:   Wed Oct 4 17:19:14 2017 -0300

    primeiro commit
